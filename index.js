

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const chessWidth = 30; //棋子宽度
const size = 15; // 棋盘
let arr = [];
let backArr = [];
let temparr = [];
let countNum = 0;
canvas.width = canvas.height = chessWidth * (size + 1);

drawChessboard();

/**
 *点击棋盘 添加棋子
 * 1.判断是否超过边界
 * 2.当前点是否存在棋子
 */
function play(e) {
  let x = Math.round(e.layerX / chessWidth) * chessWidth;
  let y = Math.round(e.layerY / chessWidth) * chessWidth;
  let color = arr.length % 2 ? 'black' : 'white';

  if (isInOut(x, y)) {
    return;
  }

  if (isExist(x, y)) {
    return;
  }
  arr.push({ x, y, color });
  drawChess({ x, y, color });
}

// 悔棋
function back() {
  if (!arr.length) {
    alert('没有棋子了');
    return;
  }
  arr.pop();
  //   重绘
  redraw(arr);
}

// 重新开始
function start() {
  arr = [];
  redraw(arr);
}

// 回顾
function lookBack() {
  backArr = arr;
  arr = [];
  redraw(arr);
}

// 上一步
function preview() {
  temparr.pop();
  redraw(temparr);
}

//下一步
function next() {
  countNum++;
  temparr = backArr.slice(0, countNum);
  redraw(temparr);
}

// 画棋盘
function drawChessboard() {
  for (let i = 1; i <= size; i++) {
    ctx.beginPath();
    ctx.moveTo(chessWidth, i * chessWidth);
    ctx.lineTo(chessWidth * size, i * chessWidth);

    ctx.moveTo(i * chessWidth, chessWidth);
    ctx.lineTo(i * chessWidth, chessWidth * size);
    ctx.closePath();
    ctx.stroke();
  }
}

//画棋子
function drawChess(item) {
  ctx.beginPath();
  ctx.arc(item.x, item.y, chessWidth / 2, 0, 2 * Math.PI, false);
  ctx.fillStyle = item.color;
  ctx.fill();
  ctx.stroke();
}

// 是否超过边界
function isInOut(x, y) {
  if (x < size || x > chessWidth * size || y < size || y > chessWidth * size) {
    alert('请不要在棋盘外放置棋子');
    return true;
  }
  return false;
}

// 棋子是否已存在
function isExist(x, y) {
  let newArr = arr.filter((item) => {
    return item.x == x && item.y == y;
  });
  return newArr.length;
}

//重绘
function redraw(params) {
  ctx.clearRect(
    chessWidth / 2,
    chessWidth / 2,
    chessWidth * size,
    chessWidth * size
  );
  drawChessboard();
  params.forEach(drawChess);
}
